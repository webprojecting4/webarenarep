<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class PlayersController extends AppController
{
//    public $components = array('Auth');
    
    
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('register', 'logout', 'forgetPassword');
    }
    
    public function register()
    {
        $player = $this->Players->newEntity();
        if ($this->request->is('post')) {
            $player = $this->Players->patchEntity($player, $this->request->getData());
            if ($this->Players->save($player)) {
                $this->Flash->success(__("Utilisateur ajouté"));
                return $this->redirect(['controller' => 'Arenas','action' => 'index']);
            }
            $this->Flash->error(__("Problème ajout utilisateur"));
        }
        $this->set('player', $player);
    }
    
    public function login()
    {
        if ($this->request->is('post')) {
            $player = $this->Auth->identify();
            if ($player) {
                $this->Auth->setUser($player);
                 return $this->redirect(['controller' => 'Arenas','action' => 'index']);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    
    public function index ()
    {
        $this->set('players', $this->Players->find('all'));
    }
    
    public function forgetPassword()
    {
        $this->loadModel('Players');
        if ($this->request->is('post')) {
        debug($this->request->data);
        
        
        $this->Players->changePlayerPassword($this->request->data['email'], $this->request->data['password']);
            
        }
    }


    
    
}