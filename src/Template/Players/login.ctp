<?php $this->assign('title','Login');?>

<section class = "form-signin well col-sm-offset-3 col-sm-6 text-center">
    <h2> Form Login </h2>
    <?= $this->Form->create()?>  
    <fieldset>
        <?= 
            $this->Form->control('email', [
                'type' => 'email',
                "class" => "form-control",
                "placeholder" => "Email Adress",
                "label" => false
                ])
        ?>
        <?=        
            $this->Form->password("password", [
                "class" => "form-control",
                "placeholder" => "Password"
                ])

        ?>
    </fieldset>
    <?= $this->Html->link('Forgot Password', array('controller' => 'Players', 'action' => 'forgetPassword'));?>
    <?= 
        $this->Form->button("Login", [
            "type" => "submit",
            "class" => "btn btn-lg btn-primary btn-block",
            "name" => "Login"
      ])

    ?>

    <?= $this->Form->end()?>        
</section> 

    <script type="text/javascript">
        var page = document.getElementById("login");
        page.className="active";
    </script>
