<?php $this->assign('title','Login');?>
<section class = "form-signin well col-sm-offset-3 col-sm-6 text-center">
    <h2> Change Password </h2>


        <?= $this->Form->create()?>  
        <fieldset>
            <?= 
                $this->Form->control('email', [
                    'type' => 'email',
                    "class" => "form-control",
                    "placeholder" => "Email Adress",
                    "label" => false
                    ])
            ?>
            <?=        
                $this->Form->password("password", [
                    "class" => "form-control",
                    "placeholder" => "New Password (8 min)"
                    ])

            ?>
        </fieldset>
        <?= 
            $this->Form->button("Change Password", [
                "type" => "submit",
                "class" => "btn btn-lg btn-primary btn-block",
                "name" => "changepass"
          ])
        ?>
        <?= $this->Form->end()?>        
</section>
    <script type="text/javascript">
        var page = document.getElementById("register");
        page.className="active";
    </script>
    
