<?php $this->assign('title','Index');
    if ($this->request->session()->read('Auth.User.id'))
    {
        $connected=true;
        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
    }
    else {$connected=false;}
 ?>
    
    
    <div class="row jumbotron">
            <div class="col-lg-7">
                <h1>Web Arena</h1>
                <p>Welcome to this online game, create now your fighter and be ready to fight in the Arena !</p>
                <?php 
                    if (!$connected)
                    {
                        echo $this->Html->link('Register', array('controller' => 'Players', 'action' => 'register'), ["class" => "btn btn-lg btn-default"]);
                        echo $this->Html->link('Login', array('controller' => 'Players', 'action' => 'login'), ["class" => "btn btn-lg btn-default"]);
                    }
                ?>
                
            </div>
            <div class="col-lg-5">
                <?= $this->Html->image("logo.png", ['class' => "img-responsive", 'alt' => 'logo_webarena'])?>
            </div>        
    </div>

    <script type="text/javascript">
        var page = document.getElementById("index");
        page.className="active";
    </script>