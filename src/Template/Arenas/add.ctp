<section class = "form-signin well col-sm-offset-3 col-sm-6 col-xs-12">

        <h2 class="text-center">Ajouter un combattant</h2>
            <?php

                echo $this->Form->create(NULL, ['type' => 'file', "class" => "col-xs-offset-1 col-xs-10"]);
                echo $this->Form->file('submittedfile', ['accept' => 'image/*', "class" => "form-control-file", "required" => "true"]);
                echo $this->Form->input('name' , ["class" => "form-control", "placeholder" => "Fighter's name ", "label" => false, "required" => "true"]);
                echo $this->Form->button(__('Add Fighter'), ["class" => " btn-primary btn-block"]);
                echo $this->Form->end();
            ?>

</section>

