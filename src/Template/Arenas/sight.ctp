<?php $this->assign('title','Sight');?>

    <section class="container-fluid">
        <table class="col-sm-8 table-condensed">
            <?php   
                    for($i=0; $i<$lig;$i++)
                    {
                        echo('<tr>');
                        for($j=0;$j<$col;$j++) //pour chaque case
                        {
                            echo('<td>');
                            
                            if(abs($fighter->coordinate_x-$j)+abs($fighter->coordinate_y-$i)>$fighter->skill_sight) 
                                //en dehors du champ de vision
                            {
                                ?> 
                                    <img class="img-responsive" src="../webroot/img/fog.png" alt="fog">
                                <?php
                            }
                            // A l'intérieur du champ de vision
                            if(abs($fighter->coordinate_x-$j)+abs($fighter->coordinate_y-$i)<=$fighter->skill_sight
                               &&!($j==$fighter->coordinate_x&&$i==$fighter->coordinate_y))
                            {
                                $test = false;
                                $test2 = true;
                                $t=0;
                                $l=0;

                                    foreach($pos_y as $y)
                                    {
                                        foreach($pos_x as $x)
                                        { 
                                            if($l == $t)
                                            {
                                                //Autres combattants
                                                if($j == $x->coordinate_x && $i == $y->coordinate_y)
                                                {
                                                    echo ($this->Html->image('logo/' . $id_enemy[$l]->player_id . ".png", ['class' => "img-responsive img_avatar", 'alt' => 'Logo', "width" => "50px", "height" => "50px"]));
                                                    $test=true;
                                                }
                                            }
                                            $l += 1;
                                        }
                                        $l=0;
                                        $t += 1;
                                    }

                                
    
                                //Champ de vision hors combattants
                                if(!$test)
                                {
                                   
                                    foreach($Surroundings as $s)
                                    {
                                        if($j==$s->coordinate_x && $i==$s->coordinate_y && $s->type == "Wall") //perso
                                        {
                                            ?> 
                                                <img class="img-responsive" src="../webroot/img/wall.png" alt="floor">
                                            <?php
                                            $test2 = false;
                                        }
                                        if($j==$s->coordinate_x && $i==$s->coordinate_y && $s->type == "Trap") //perso
                                        {
                                            echo ($this->Html->image("floor.png", ['class' => "img-responsive", 'alt' => 'floor']));
                                            $test2 = false;
                                        }
					if($j==$s->coordinate_x && $i==$s->coordinate_y && $s->type == "Wampus") //perso
                                        {
                                            echo ($this->Html->image("floor.png", ['class' => "img-responsive", 'alt' => 'floor']));
                                            //echo 'W'; 
                                            $test2 = false;
                                        }
                                    }
                                       
                                    
                                    if($test2)
                                    {
                                        echo ($this->Html->image("floor.png", ['class' => "img-responsive", 'alt' => 'floor']));

                                    }
                                    $test2 = true;
                                }
                                $test = false;
                            }
                            if($j==$fighter->coordinate_x&&$i==$fighter->coordinate_y) //perso
                            {
                                if($Warning)
                                {
                                    echo ($this->Html->image("brise.png", ['class' => "img-responsive altern", 'alt' => 'brise_suspecte']));
                                    echo ($this->Html->image('logo/' . $img_fighter, ['class' => "img-responsive img_avatar altern", 'alt' => 'Logo', "width" => "50px", "height" => "50px", "style"=>"display: none"]));
                                }
                                else
                                if($Wampus)
                                {
                                    echo ($this->Html->image('puanteur.png' . $img_fighter, ['class' => "img-responsive altern", 'alt' => 'Puanteur']));
                                    echo ($this->Html->image('logo/' . $img_fighter, ['class' => "img-responsive img_avatar altern", 'alt' => 'Logo', "width" => "50px", "height" => "50px", "style"=>"display: none"]));
                                }
                                else
                                {                   
                                    echo ($this->Html->image('logo/' . $img_fighter, ['class' => "img-responsive img_avatar", 'alt' => 'Logo', "width" => "50px", "height" => "50px"]));
                                }
                            }
                            echo('</td>');
                        }
                        echo('</tr>');
                        };
            ?>
            
        </table>
       

        <section class="col-sm-4 well col-xs-12">
            
            <div class="row">
                 
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-offset-3">
                            <h3>Moves</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-3">
                            <?= 
                                $this->Html->image('up.png', [
                                    "height" => "50",
                                    "alt" => "up_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'setPlayerPosition',$fighter->id,$fighter->coordinate_x,($fighter->coordinate_y-1)]
                                ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <?= 
                                $this->Html->image('left.png', [
                                    "height" => "50",
                                    "alt" => "left_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'setPlayerPosition',$fighter->id,($fighter->coordinate_x-1),$fighter->coordinate_y]
                                ])
                            ?>
                        </div>
                        <div class="col-xs-offset-3 col-xs-3">
                            <?= 
                                $this->Html->image('right.png', [
                                    "height" => "50",
                                    "alt" => "right_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'setPlayerPosition',$fighter->id,($fighter->coordinate_x+1),$fighter->coordinate_y]
                                ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-4">
                            <?= 
                                $this->Html->image('down.png', [
                                    "height" => "50",
                                    "alt" => "down_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'setPlayerPosition',$fighter->id,$fighter->coordinate_x,($fighter->coordinate_y+1)]
                                ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-offset-2 col-xs-6">
                            <h3>Attacks</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-3">
                            <?= 
                                $this->Html->image('sword_up.png', [
                                    "height" => "50",
                                    "alt" => "down_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'killEnemy',$fighter->id,$fighter->coordinate_x,$fighter->coordinate_y,"haut"]
                                ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-0 col-xs-3">
                            <?=
                                $this->Html->image('sword_left.png', [
                                        "height" => "50",
                                        "alt" => "sword_left_icon",
                                        'url' => ['controller' => 'Arenas', 'action' => 'killEnemy',$fighter->id,$fighter->coordinate_x,$fighter->coordinate_y,"gauche"]
                                    ])
                            ?>
                        </div>
                        <div class="col-xs-offset-3 col-xs-3">
                            <?=
                                $this->Html->image('sword_right.png', [
                                        "height" => "50",
                                        "alt" => "sword_right_icon",
                                        'url' => ['controller' => 'Arenas', 'action' => 'killEnemy',$fighter->id,$fighter->coordinate_x,$fighter->coordinate_y,"droite"]
                                    ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-4">
                            <?=
                                $this->Html->image('sword_down.png', [
                                    "height" => "50",
                                    "alt" => "sword_down_icon",
                                    'url' => ['controller' => 'Arenas', 'action' => 'killEnemy',$fighter->id,$fighter->coordinate_x,$fighter->coordinate_y,"bas"]
                                ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-offset-4 col-xs-4">
                    <?= $this->Html->image("icones/".$time."pa.png", ['class' => "img-responsive", 'alt' => 'action point icon'])?>
                </div>
            </div>
            <div class="row">
                <?= $this->Html->link('Surroundings', ['action' => 'generateSurroundings'], ["class" => "btn btn-primary btn-block btn-success"]);//,$fighter->id,$fighter->coordinate_x,$fighter->coordinate_y]);?>
            </div>
        </section>
    </section>

  <script type="text/javascript">
        var page = document.getElementById("sight");
        page.className="active";
    </script> 
    <script>
        var tid = setInterval(mycode, 1000);
        function mycode() {
          $('.altern').toggle();
        }
        
        
        
    </script>