<?php
namespace App\Controller;
use App\Controller\AppController;
use App\Model\Table\FightersTable;
use App\Model\Table\EventsTable;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\I18n\Time;
/**
* Personal Controller
* User personal interface
*
*/
class ArenasController  extends AppController
{
    public function index()
    {
    }
    
    public function fighter()
    {

        $id = $this->request->session()->read('Auth.User')['id'];
        $this->loadModel('Fighters');
        $dir = new Folder(WWW_ROOT . 'img/logo');
        $img = $dir->find($id.'.png', true);
        $dir2 = new Folder(WWW_ROOT . 'img/min');
        $img2 = $dir2->find($id.'.png', true);
        $path = $dir->path;

        // Regarder s'il existe un combattant		
        try
        {
            $result = $this->Fighters->getFighterById($id);
        }
        catch(RecordNotFoundException $e)
        {
            $result = [];
        }

        if(!$result)
        {
            return $this->redirect(['action' => 'add']);
        }
        else
        {
            //En cas de mort ajouter un nouveau combattant
            if($result[0]->current_health == 0)
                return $this->redirect(['action' => 'add',$result[0]->id]);

            $this->set('fighter',$result);
            $this->set('img',$img[0]);
        }
        if($this->request->is(['post', 'put'])) 
        {
            debug($this->request->data);

                $choice=$this->request->data('skill');
                //debug($choice);
                $skill_sight=0;
                $skill_strength=0;
                $skill_health=0;

                if($choice == 0)
                {
                    $skill_sight=1;
                }
                else if($choice == 1)
                {
                    $skill_strength=1;
                }
                else if($choice == 2)
                {
                    $skill_health=1;
                }
                $this->Fighters->editFighter($result[0], $skill_sight, $skill_strength, $skill_health);
                $this->Fighters->changeFighterLevel($result[0]->id);
                return $this->redirect(['action' => 'fighter']);

            
            
        }  
                
    }
    
    public function sight()
    {
        $this->loadModel('Surroundings');
        $Surroundings = $this->Surroundings->getSurroundings();
		//$id = 1;
		$id = $this->request->session()->read('Auth.User')['id'];
        $this->loadModel('Fighters');
        
        // Garder pour la selection du combattant en cours
        //$result = $this->Fighters->get($id);
		//$result = $this->Fighters->getFighterById($id);
        
        // Regarder s'il existe un combattant
        try
        {
            $result = $this->Fighters->getFighterById($id);
        }
        catch(RecordNotFoundException $e)
        {
            $result = [];
        }

        if(!$result)
        {
            return $this->redirect(['action' => 'add']);
        }
        
        //Récupérer les coordonnées des autres fighters
        list ($pos_x, $pos_y, $id_enemy) = $this->Fighters->getPositionFighters();//$id);
        $this->set('pos_x',$pos_x);
        $this->set('pos_y',$pos_y);
        $this->set('id_enemy',$id_enemy);
        
        //Récupérer la taille de la grille
        list ($lig, $col) = $this->Fighters->getMaxSize();
        
         //Récupérer l'image du fighter
        $dir = new Folder(WWW_ROOT . 'img/logo');
        $img = $dir->find($id.'.png', true);
        
        $this->set('img_fighter',$img[0]);
        $this->set('fighter',$result[0]);
        $this->set('lig',$lig);
        $this->set('col',$col);
        $this->set('Surroundings',$Surroundings);
        $this->set('Warning',false);
        $this->set('Wampus',false);
        
        
        list ($timePeriod, $maxActionPoint) = $this->Fighters->getMaxTime();
       
        
        $timeDiff = $result[0]->next_action_time->diff(Time::now());
        $diff = $timeDiff->h*3600 + $timeDiff->i*60 + $timeDiff->s;
        $this->set('diff',$diff);
        $this->set('test',$result[0]->next_action_time);
        $this->set('now',Time::now());
        
        if($diff/$timePeriod > $maxActionPoint){
            $diff = $maxActionPoint*$timePeriod;
        }
        $this->set('time',(int)($diff/$timePeriod));
        
        
        $Warning_up = $this->Surroundings->whatIsOnThisSquare($result[0]->coordinate_x,$result[0]->coordinate_y-1);
        $Warning_down = $this->Surroundings->whatIsOnThisSquare($result[0]->coordinate_x,$result[0]->coordinate_y+1);
        $Warning_left = $this->Surroundings->whatIsOnThisSquare($result[0]->coordinate_x-1,$result[0]->coordinate_y);
        $Warning_right = $this->Surroundings->whatIsOnThisSquare($result[0]->coordinate_x+1,$result[0]->coordinate_y);
        
        if($Warning_up)
        {
            if($Warning_up->type == "Trap")
            {
                $this->set('Warning',true);
            }
            else if($Warning_up->type == "Wampus")
            {
                    $this->set('Wampus',true);
            }
        }
        if($Warning_down)
        {
            if($Warning_down->type == "Trap")
            {
                $this->set('Warning',true);
            }
            else if($Warning_down->type == "Wampus")
            {
                    $this->set('Wampus',true);
            }			
        }
        if($Warning_right)
        {
            if($Warning_right->type == "Trap")
            {
                $this->set('Warning',true);
            }
            else if($Warning_right->type == "Wampus")
            {
                    $this->set('Wampus',true);
            }
        }
        if($Warning_left)
        {
            if($Warning_left->type == "Trap")
            {
                $this->set('Warning',true);
            }
            else if($Warning_left->type == "Wampus")
            {
                    $this->set('Wampus',true);
            }
        }
        
    }
    
    public function login()
    {
    }
    
    public function diary()
    {
        $this->loadModel('Events');
        $id = $this->request->session()->read('Auth.User')['id'];
        $this->loadModel('Fighters');
        
        // Regarder s'il existe un combattant
        try
        {
            $result = $this->Fighters->getFighterById($id);
        }
        catch(RecordNotFoundException $e)
        {
            $result = [];
        }

        if(!$result)
        {
            return $this->redirect(['action' => 'add']);
        }
        
        $this->set('diary',$this->Events->getEvent());
        $this->set('fighter',$result[0]);


    }
    
    public function edit($id = null)
    {
        $this->loadModel('Fighters');
        $fighter = $this->Fighters->get($id);
        $this->set('fighter', $fighter);
        
        //Tester si le combattant doit monter de niveau
        if(!(($fighter->xp/4 - $fighter->level) == 0  && $fighter->level !=0))
             return $this->redirect(['action' => 'fighter']);
		
		//Ajout du niveau
        $this->Fighters->changeFighterLevel($id);
        
        // Vérification de la méthode (POST ou PUT)
        if ($this->request->is(['post', 'put'])) {
            
            //Récupération des données
            $result = $this->request->getData();
            
            //On ne peut augmenter qu'une compétance
            if($result['skill_sight'] + $result['skill_strength'] + $result['skill_health'] == 1)
            {
                //Sauvegarder les changements en base de données
                if ($this->Fighters->editFighter($fighter,$result['skill_sight'] , $result['skill_strength'], $result['skill_health'])) {
                    
                    $this->Flash->success(__('Votre combattant a été mis à jour.'));
                    
                    //Redirection vers la page d'info du combattant
                    return $this->redirect(['action' => 'fighter']);
                }
                $this->Flash->error(__('Impossible de mettre à jour votre combattant.'));
            }
            $this->Flash->error(__('Veuillez selectionner un seul champ'));
            
        }
    }
    
    public function add($id = null)
    {
	$id = $this->request->session()->read('Auth.User')['id'];
        $this->loadModel('Fighters');
        
        // Vérification de la méthode (POST ou PUT)
        if ($this->request->is(['post', 'put'])) 
        {
            //Récupération des données
            $result = $this->request->getData();
            $file=$result['submittedfile'];
            
            //Sauvegarder les changements en base de données
            if ($this->Fighters->saveFighter($result['name'],$id))
            { 
                $this->Flash->success(__('Votre combattant a été mis à jour.'));
                
                //upload File
                $move_url = WWW_ROOT . "img/logo/" . $result['submittedfile']['name'];
                debug($move_url);
                if(move_uploaded_file($file['tmp_name'], $move_url))
                {
                    $this->Flash->success(__('Upload effectué avec succès !'));
                }
                else //Sinon (la fonction renvoie FALSE).
                {
                            $this->Flash->error(__('Echec de l\'Upload'));
                }
                $this->changeLogo($result['submittedfile']['name']);
            }
            else
                $this->Flash->error(__('Impossible de mettre à jour votre combattant.'));
        }
    }
    
    public function newLogo()
    {
        if($this->request->is(['post', 'put'])) 
        {
            debug($this->request->data);
            $result = $this->request->getData();
            $file=$result['submittedfile'];
            //upload File
            $move_url = WWW_ROOT . "img/logo/" . $result['submittedfile']['name'];
            debug($move_url);
            if(move_uploaded_file($file['tmp_name'], $move_url))
            {
                $this->Flash->success(__('Upload effectué avec succès !'));
            }
            else //Sinon (la fonction renvoie FALSE).
            {
                        $this->Flash->error(__('Echec de l\'Upload'));
            }
            $this->changeLogo($result['submittedfile']['name']);
        }
    }
    
	
	public function isPositionAllowed($coord_x, $coord_y , $id)
    {
        $this->loadModel('Fighters');
        $result = $this->Fighters->get($id);
        list ($pos_x, $pos_y) = $this->Fighters->getPositionFighters();//$id);
		$this->loadModel('Surroundings');
        $Surroundings = $this->Surroundings->getSurroundings();
		$t=0;
        $l=0;

        $end=true;
        foreach($pos_y as $py)
        {
            foreach($pos_x as $px)
            { 
                if($l == $t)
                {
                    if($coord_x != $result->coordinate_x || $coord_y != $result->coordinate_y)
                    {
                         if($coord_x == $px->coordinate_x && $coord_y == $py->coordinate_y)
                        {
                             return false;
                        }
                    }
                   
                }
                $l += 1;
            }
            $l=0;
            $t += 1;
        }
		foreach($Surroundings as $s)
        {
			if($coord_x == $s->coordinate_x && $coord_y == $s->coordinate_y)
			{
				return false;
			}           
        }
        return true;
    }
    public function setFighterLevel($id)
    {
        //$this->loadModel('Fighters');
        
        //Ajout du niveau
        //$this->Fighters->changeFighterLevel($id);
        
        //Redirection vers la page d'ajout de compétances
        $this->redirect(['action' => 'edit',$id]);
        
    }
	
    public function changeLogo($num)
    {
        //Récupérer l'Id de l'utilisateur
        $id = $this->request->session()->read('Auth.User')['id'];

        //Copier le logo choisi et changer le nom
        $dir = new Folder(WWW_ROOT . 'img/logo/');
		
        $path = $dir->path;
	debug($num);
        debug($path);
        copy($path.$num,$path.$id.'.png');
        
        return $this->redirect(['action' => 'fighter']);
        
    }
    public function setPlayerPosition($id, $coord_x, $coord_y)
    {
        $this->loadModel('Fighters');
		$this->loadModel('Surroundings');
        $Surroundings = $this->Surroundings->getSurroundings();
		
		foreach($Surroundings as $s)
        {
			if($coord_x == $s->coordinate_x && $coord_y == $s->coordinate_y)
			{
				if($s->type == "Trap")
				{
					$this->deleteFighter($id);
					return $this->redirect(['action' => 'sight']);
				}
				else
					if($s->type == "Wampus")
					{
						$this->deleteFighter($id);
						return $this->redirect(['action' => 'sight']);
					}
			}
            
        }
        
        //Vérifier que le combattant est dans la grille
        if($coord_x>=0 && $coord_x<15 && $coord_y>=0 && $coord_y<10 && $this->isPositionAllowed($coord_x, $coord_y, $id))
        {
            //Changer la positoin du joueur
            
            if($this->actionPointProcess($id))
            {
                $this->Fighters->changePlayerPosition($id,$coord_x,$coord_y);
                return $this->redirect(['action' => 'sight']);
            }
            else
            {
                $this->Flash->error(__('Wait for more Action Points'));
                return $this->redirect(['action' => 'sight']);
            }
        }
        $this->Flash->error(__('Impossible de déplacer votre combattant.'));
        return $this->redirect(['action' => 'sight']);
    }
    /*
    public function addEvent ($x,$y)
    {
        $this->loadModel('Events');
        $this->Events->saveEvent($x, $y, "Je suis un event");
        return $this->redirect(['action' => 'sight']);
    }*/
	public function deleteFighter ($id)
	{
		$this->loadModel('Fighters');
		$fighter = $this->Fighters->get($id);
		$result = $this->Fighters->delete($fighter);
		return $this->redirect(['action' => 'fighter']);
	}

    public function killEnemy($id,$coord_x, $coord_y,$direction)
    {
        $this->loadModel('Fighters');
        $this->loadModel('Events');
        $this->loadModel('Surroundings');
		
		//Récupérer la taille de la grille
        list ($lig, $col) = $this->Fighters->getMaxSize();
		if($this->actionPointProcess($id))
        {
            if($direction == "haut")
            {
                if($coord_y >0)      //haut 
                {
                    if(!$this->isPositionAllowed($coord_x,$coord_y-1,$id))
                    {
                        $surrounding = $this->Surroundings->whatIsOnThisSquare($coord_x,$coord_y-1);
                        if($surrounding)
                        {
                            if($surrounding->type =="Wampus")
                            {
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché le Wampus");
                                $this->Surroundings->delete($surrounding);
                                $this->redirect(['action' => 'sight']);
                            }
                            else
                            {
                                if($surrounding->type != "Wampus")
                                {
                                    $this->redirect(['action' => 'sight']);
                                }
                            }
                        }
                        else
                        {						
                            if($this->Fighters->attackSuccess($coord_x,$coord_y-1,$id))
                            {
                                $this->Fighters->processDamage($coord_x,$coord_y-1,$id);
                                $enemy_id =$this->Fighters->whoIsOnThisSquare($coord_x,$coord_y-1)->id;
                                $this->Flash->success(__('hit'));
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché ".$this->Fighters->getFighterName($enemy_id));
                                if($this->Fighters->isDead($enemy_id))
                                {
                                    $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a tué ".$this->Fighters->getFighterName($enemy_id));
                                    $this->Fighters->addXpWhenDead($coord_x,$coord_y-1,$id);
                                    $result = $this->Fighters->delete($this->Fighters->whoIsOnThisSquare($coord_x,$coord_y-1));
                                }
                            }
                            else
                            {
                                $this->Flash->error(__('missed'));
                            }
                        }
                    }
                }
            }
            if($direction == "droite")
            {
                 
                if($coord_x < $col-1) //droite
                {
                    if(!$this->isPositionAllowed($coord_x+1,$coord_y,$id))
                    {
                        $surrounding = $this->Surroundings->whatIsOnThisSquare($coord_x+1,$coord_y);
                        if($surrounding)
                        {
                            if($surrounding->type =="Wampus")
                            {
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché le Wampus");
                                $this->Surroundings->delete($surrounding);
                                $this->redirect(['action' => 'sight']);
                            }
                            else
                            {
                                if($surrounding->type != "Wampus")
                                {
                                    $this->redirect(['action' => 'sight']);
                                }
                            }
                        }
                        else
                        {
                            if($this->Fighters->attackSuccess($coord_x+1,$coord_y,$id))
                            {
                                $this->Fighters->processDamage($coord_x+1,$coord_y,$id);
                                $enemy_id =$this->Fighters->whoIsOnThisSquare($coord_x+1,$coord_y)->id;
                                $this->Flash->success(__('hit'));
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché ".$this->Fighters->getFighterName($enemy_id));
                                if($this->Fighters->isDead($enemy_id))
                                {
                                    $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a tué ".$this->Fighters->getFighterName($enemy_id));
                                    $this->Fighters->addXpWhenDead($coord_x+1,$coord_y,$id);
                                    $result = $this->Fighters->delete($this->Fighters->whoIsOnThisSquare($coord_x+1,$coord_y));
                                }
                            }
                            else
                            {
                                $this->Flash->error(__('missed'));
                            }
                        }
                    }
                }   
            } 

            if($direction == "bas")
            {
                if($coord_y < $lig-1)   //bas
                {
                    if (!$this->isPositionAllowed($coord_x,$coord_y+1,$id))
                    {
                        $surrounding = $this->Surroundings->whatIsOnThisSquare($coord_x,$coord_y+1);
                        if($surrounding)
                        {					
                            if($surrounding->type == "Wampus")
                            {
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché le Wampus");
                                $this->Surroundings->delete($surrounding);
                                $this->redirect(['action' => 'sight']);
                            }
                            else
                            {
                                if($surrounding->type != "Wampus")
                                {
                                    $this->redirect(['action' => 'sight']);
                                }
                            }
                        }

                        else
                        {
                            if($this->Fighters->attackSuccess($coord_x,$coord_y+1,$id))
                            {
                                $this->Fighters->processDamage($coord_x,$coord_y+1,$id);
                                $enemy_id =$this->Fighters->whoIsOnThisSquare($coord_x,$coord_y+1)->id;
                                $this->Flash->success(__('hit'));
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché ".$this->Fighters->getFighterName($enemy_id));
                                if($this->Fighters->isDead($enemy_id))
                                {
                                    $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a tué ".$this->Fighters->getFighterName($enemy_id));
                                    $this->Fighters->addXpWhenDead($coord_x,$coord_y+1,$id);
                                    $result = $this->Fighters->delete($this->Fighters->whoIsOnThisSquare($coord_x,$coord_y+1));
                                }
                            }
                            else
                            {
                                $this->Flash->error(__('missed'));
                            }
                        }
                    }
                }  
            }

            if($direction == "gauche")
            {
                if($coord_x >0) //gauche
                {
                    if(!$this->isPositionAllowed($coord_x-1,$coord_y,$id))
                    {
                        $surrounding = $this->Surroundings->whatIsOnThisSquare($coord_x-1,$coord_y);
                        if($surrounding)
                        {
                            if($surrounding->type == "Wampus")
                            {	
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché le Wampus");
                                $this->Surroundings->delete($surrounding);
                                $this->redirect(['action' => 'sight']);
                            }
                            else
                            {
                                if($surrounding->type != "Wampus")
                                {
                                    $this->redirect(['action' => 'sight']);
                                }
                            }
                        }
                        else
                        {
                            if($this->Fighters->attackSuccess($coord_x-1,$coord_y,$id))
                            {
                                $this->Fighters->processDamage($coord_x-1,$coord_y,$id);
                                $enemy_id =$this->Fighters->whoIsOnThisSquare($coord_x-1,$coord_y)->id;
                                $this->Flash->success(__('hit'));
                                $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a touché ".$this->Fighters->getFighterName($enemy_id));
                                if($this->Fighters->isDead($enemy_id))
                                {
                                    $this->Events->saveEvent($coord_x,$coord_y, $this->Fighters->getFighterName($id)." a tué ".$this->Fighters->getFighterName($enemy_id));
                                    $this->Fighters->addXpWhenDead($coord_x-1,$coord_y,$id);
                                    $result = $this->Fighters->delete($this->Fighters->whoIsOnThisSquare($coord_x-1,$coord_y));
                                }
                            }
                            else
                            {
                                $this->Flash->error(__('missed'));
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $this->Flash->error(__('Wait for new action points'));
        }
        $this->redirect(['action' => 'sight']);
 
    }
    public function generateSurroundings()
    {
        $l = 0;
        $t = 0;
        $this->loadModel('Fighters');
        //Récupérer la taille de la grille
        list ($lig, $col) = $this->Fighters->getMaxSize();
        list ($pos_x, $pos_y) = $this->Fighters->getPositionFighters();
		
        $end = true;
        $max = (($lig*$col)/10);
        $this->loadModel('Surroundings');
        for($i=0;$i<$max;$i++)
        { 
            $Surroundings = $this->Surroundings->getSurroundings();
            $l = 0;
            $t = 0;
            while($end)
            {
                $x=rand(0,$col-1);
                $y=rand(0,$lig-1);
                $end=false;
                foreach($pos_y as $py)
                {
                    foreach($pos_x as $px)
                    { 
                        if($l == $t)
                        {
                            if($x == $px->coordinate_x && $y == $py->coordinate_y)
                            {
                                 $end = true;
                            }
                        }
                        $l += 1;
                    }
                    $l=0;
                    $t += 1;
                }
                foreach($Surroundings as $s)
                {
                    if($x == $s->coordinate_x && $y == $s->coordinate_y)
                    {
                         $end = true;
                    }
                }
				
            }
            $this->Surroundings->createWall($x,$y,$i+1);
            
            $Surroundings = $this->Surroundings->getSurroundings();
			$l = 0;
        	$t = 0;
            $end = true;
            while($end)
            {
                $x=rand(0,$col-1);
                $y=rand(0,$lig-1);
                $end=false;
                foreach($pos_y as $py)
                {
                    foreach($pos_x as $px)
                    { 
                        if($l == $t)
                        {
                            if($x == $px->coordinate_x && $y == $py->coordinate_y)
                            {
                                 $end = true;
                            }
                        }
                        $l += 1;
                    }
                    $l=0;
                    $t += 1;
                }
                foreach($Surroundings as $s)
                {
                    if($x == $s->coordinate_x && $y == $s->coordinate_y)
                    {
                         $end = true;
                    }
                }
				
            }
            $this->Surroundings->createTrap($x,$y,$i+$max+1);
            

            $end = true;
        }
		
		$Surroundings = $this->Surroundings->getSurroundings();
		while($end)
		{
			$x=rand(0,$col-1);
			$y=rand(0,$lig-1);
			$end=false;
			foreach($pos_y as $py)
			{
				foreach($pos_x as $px)
				{ 
					if($l == $t)
					{
						if($x == $px->coordinate_x && $y == $py->coordinate_y)
						{
							 $end = true;
						}
					}
					$l += 1;
				}
				$l=0;
				$t += 1;
			}
			foreach($Surroundings as $s)
			{
				if($x == $s->coordinate_x && $y == $s->coordinate_y)
				{
					 $end = true;
				}
			}
		}
		$this->Surroundings->createWampus($x,$y,2*$max+1);

        $this->redirect(['action' => 'sight']);
    }
    
    public function actionPointProcess($id)
    {   
        $this->loadModel('Fighters');
        $fighter = $this->Fighters->get($id);
        list ($timePeriod, $maxActionPoint) = $this->Fighters->getMaxTime();

        $timeDiff = $fighter->next_action_time->diff(Time::now());
        $diff = ($timeDiff->h*3600 + $timeDiff->i*60 + $timeDiff->s)/$timePeriod;

        if($diff > $maxActionPoint){
            $time_temp = Time::now();
            $time_temp->modify('-'.($maxActionPoint * $timePeriod).' seconds');
            $this->Fighters->setActionReferenceDate($id,$time_temp);
        }
        if($diff < 1 ||$timeDiff->invert){
            return false;
        }
        else {
            $fighter_2 = $this->Fighters->get($id);
            //$time_temp_2 = new Time($fighter_2->next_action_time->i18nFormat('yyyy-MM-dd HH:mm:ss'));
            $fighter_2->next_action_time = $fighter_2->next_action_time->modify('+'.$timePeriod.' seconds');
            $this->Fighters->setActionReferenceDate($id,$fighter_2->next_action_time);
            return true;
        }
        return true;

    }
}
