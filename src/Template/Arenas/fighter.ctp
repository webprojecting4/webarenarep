<?php $this->assign('title','Fighter');?>
<!DOCTYPE html>
<html>
<head>
    
</head>
<body>
    
    <section class="container-fluid">
        <section class="row well">
            <?php foreach ($fighter as $f): ?>
            
            <div class="row">
                <div class="col-sm-offset-1 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-sm-offset-0 col-sm-4 col-xs-offset-2 col-xs-3">
                            <div  data-toggle="tooltip" data-placement="bottom" title="Your Level"><?= $this->Html->image("icones/star".$f->level.".png", ['class' => "img-responsive", 'alt' => 'star_icon'])?></div>
                        </div>

                        <div class="col-sm-6 col-xs-3">
                            <h3><?= $f->name?></h3>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-offset-2 col-sm-offset-0 col-xs-8">
                            <?= $this->Html->image('logo/' . $img, ['class' => "img-thumbnail", 'alt' => 'Logo']);?>
                        </div>
                    </div>

                </div>
            
                <div class="col-sm-3 col-xs-6">                        
                    <h3> Statistics </h3>
                    <div class="row">
                        <div class="col-sm-4  col-xs-6">
                            <div  data-toggle="tooltip" data-placement="left" title="Your Strength"><?= $this->Html->image("icones/fist.png", ['class' => "img-responsive", 'alt' => 'fist_icon'])?></div>
                        </div>
                        <div class="col-sm-3  col-xs-6">
                            <h4><?= $f->skill_strength?></h4>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div  data-toggle="tooltip" data-placement="left" title="Your Sight"><?= $this->Html->image("icones/eye.png", ['class' => "img-responsive", 'alt' => 'eye_icon'])?></div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <h4> <?= $f->skill_sight?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div  data-toggle="tooltip" data-placement="left" title="Your Health"><?= $this->Html->image("icones/heart.png", ['class' => "img-responsive", 'alt' => 'heart_icon'])?></div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <h4><?= $f->current_health?> / <?= $f->skill_health?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div  data-toggle="tooltip" data-placement="left" title="Your XP Points"><?= $this->Html->image("icones/xp.png", ['class' => "img-responsive", 'alt' => 'xp_icon'])?></div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-2 col-xs-offset-1 col-xs-4">
                            <h4><?= $f->xp?></h4>
                        </div>
                    </div>


                </div>
                <div class="col-sm-4 col-xs-6">
                    <h3>Actions</h3>
                    <div class="btn-group-vertical"> 
                        <?= $this->Html->link('Delete', ['action' => 'deleteFighter',$f->id], ["class" => "btn btn-primary btn-block btn-danger "] );?>
                        <h4> Change Avatar </h4>
                        <?php
                            echo $this->Form->create(NULL, ['type' => 'file', "url" => "/arenas/newLogo"]);
                            echo $this->Form->file('submittedfile', ['accept' => 'image/*', "class" => "form-control-file", "required" => "true"]);
                            echo $this->Form->postButton('Change Logo', ["class" => " btn-primary btn-block"]);
                            echo $this->Form->end();
                        ?>
                        <?php
                        if(($f->xp/4 - $f->level) >= 0  && $f->level !=0)
                        {
                            ?>
                            <h4> Level Up </h4>
                            <?php
                            echo $this->Form->create(NULL);
                            echo $this->Form->select('skill',
                                ['skill_sight','skill_strength','skill_health'],
                                ['empty' => '(choisissez)']);

                            echo $this->Form->postButton('Level Up', ["class" => "btn btn-primary btn-block btn-success", "name" => "LevelUp"]);
                            echo $this->Form->end();

                        }
                        ?>
                    </div>
                </div>
            </div>

            
            
                    
            <br>
            <?php endforeach;?>

           
        </section>
    </section>

    
    
    <script type="text/javascript">
        var page = document.getElementById("fighter");
        page.className="active";
    </script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    </script>
    
</body>