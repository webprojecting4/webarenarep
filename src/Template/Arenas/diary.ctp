<?php $this->assign('title','Diary');?>

   
    <section class="well col-lg-offset-2 col-lg-8">
        <table class="table table-striped table-responsive table-condensed">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Coordinates (X;Y)</th>
                    <th>Date</th>
                </tr>
            </thead>
        <?php foreach ($diary as $d): ?>
            <?php         if(abs($fighter->coordinate_x-$d->coordinate_x)+abs($fighter->coordinate_y-$d->coordinate_y)<=$fighter->skill_sight)
        {
            ?>

            <tr>
                <td>
                    <?= $d->name;?>
                </td>
                <td>
                    Coord : ( <?= $d->coordinate_x;?> ; <?= $d->coordinate_y;?> )
                </td>
                <td>
                    <?= $d->date;?>
                </td>
            </tr>
<?php         } ?>
            <?php endforeach;?>
        </table>
        
    </section>

    <script type="text/javascript">
        var page = document.getElementById("diary");
        page.className="active";
    </script>
