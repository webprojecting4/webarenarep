<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
    if ($this->request->session()->read('Auth.User.id'))
    {
        $connected=true;
        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
    }
    else {$connected=false;}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <?= $this->Html->charset() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title> <?= $this->fetch('title') ?></title>
    <?= $this->Html->meta(
    'icone.ico',
    'icone.ico',
    ['type' => 'icon']
); ?>

    
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('webarena.css') ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->script('popper.min.js') ?>
    <?= $this->Html->script('jquery-3.2.1.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('tooltip.js') ?>
</head>
<body>
    <div class="container-fluid">
         <nav class="navbar navbar-default ">
             <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand" ><?php echo $this->Html->link('Web Arena', array('controller' => 'Arenas', 'action' => 'index'));?></div>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class ="nav navbar-nav ">
                        <?php
                        if (!$connected)
                        {
                            ?>
                                <li id="register"><?php echo $this->Html->link('Register', array('controller' => 'Players', 'action' => 'register'));?></li>
                                <li id="login"><?php echo $this->Html->link('Login', array('controller' => 'Players', 'action' => 'login'));?></li>
                            <?php
                        }
                        ?>
                        
                        <li id="diary"><?php echo $this->Html->link('Diary', array('controller' => 'Arenas', 'action' => 'diary'));?></li>
                        <li id="sight"><?php echo $this->Html->link('Sight', array('controller' => 'Arenas', 'action' => 'sight'));?></li>
                        <li id="fighter"><?php echo $this->Html->link('Fighter', array('controller' => 'Arenas', 'action' => 'fighter'));?></li>
                    
                        
                    
                    </ul>
                    <?php
                        if ($connected)
                        {
                            ?>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= $user_data['email']  ?><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li id="fighter"><?php echo $this->Html->link("Logout", array('controller' => 'Players', 'action' => 'logout'));?></li>
                                    </ul>
                                </li
                            </ul>
                            <?php
                        }
                    ?>
                </div>
                 
             </div>
        </nav>
    </div>
    <?= $this->Flash->render() ?>
    <div class="container clearfix main_content">
        <?= $this->fetch('content') ?>
    </div>
    <footer class="panel-footer navbar-fixed-bottom">
            <div class="col-sm-4 hidden-xs">
                <p>Victor Bigand - Stéphane Rouge - Achille Burah - Lucas Mattioli</p>
            </div>
            <div class="col-sm-3 col-xs-9">
                <p>ING 4 SI Gr03  Options CDF</p>
            </div>
            <div class="col-sm-2 col-xs-3">
                <span>
                    <a href="http://www.webarena-france.fr" class="pull-right"> Lien Site  </a>
                </span>
            </div>
            <div class="col-sm-3 col-xs-3">
                <span>
                    <a href="https://bitbucket.org/webprojecting4/webarenarep/overview" class="pull-right"> Lien Git  </a> 
		</span>
            </div>
    </footer>
</body>

    

</html>

