<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class SurroundingsTable extends Table
{
    public function createWall($coord_x,$coord_y,$id)
    {
        $SurroundingsTable = TableRegistry::get('Surroundings');
        $querry = $SurroundingsTable->find("all")->where(['Surroundings.id' => $id]);
        $Wall = $querry->first();
        if($querry->isEmpty())
        {
            $Wall = $SurroundingsTable->newEntity();
        }
        $Wall->coordinate_x = $coord_x;
        $Wall->coordinate_y = $coord_y;
        $Wall->type = "Wall";
        $SurroundingsTable->save($Wall);
    }
	public function createWampus($coord_x,$coord_y,$id)
    {
        $SurroundingsTable = TableRegistry::get('Surroundings');
        $querry = $SurroundingsTable->find("all")->where(['Surroundings.type' => "Wampus"]);
        $Wampus = $querry->first();
        if($querry->isEmpty())
        {
            $Wampus = $SurroundingsTable->newEntity();
        }
        $Wampus->coordinate_x = $coord_x;
        $Wampus->coordinate_y = $coord_y;
        $Wampus->type = "Wampus";
        $SurroundingsTable->save($Wampus);
    }
    public function createTrap($coord_x,$coord_y,$id)
    {
        $SurroundingsTable = TableRegistry::get('Surroundings');
        $querry = $SurroundingsTable->find("all")->where(['Surroundings.id' => $id]);
        $trap = $querry->first();
        if($querry->isEmpty())
        {
            $trap = $SurroundingsTable->newEntity();
        }
        $trap->coordinate_x = $coord_x;
        $trap->coordinate_y = $coord_y;
        $trap->type = "Trap";
        $SurroundingsTable->save($trap);
    }    
    public function whatIsOnThisSquare($coord_x, $coord_y)
    {
        $SurroundingsTable = TableRegistry::get('Surroundings');
        try
		{
			$x = $SurroundingsTable->find("all")->where(['coordinate_x' => $coord_x, 'coordinate_y' => $coord_y]);
            $list = $x->all()->toArray();
		}
        catch(RecordNotFoundException $e)
		{
			$list = [];
		}
        if(!$list)
        {
            return $list;
        }
        else
        {
            return $list[0];
        }
        
    }
    public function getSurroundings()
    {
        $SurroundingsTable = TableRegistry::get('Surroundings');
        $query = $SurroundingsTable->find('all');
        return $query->all();
        
    }
   
    
}