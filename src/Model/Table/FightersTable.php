<?php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class FightersTable extends Table
{
    /*
    public function getBestFighter()
    {
        $bestFighter = $this->find('all', [
                'order' => ['Fighters.level' =>'DESC']
            ])->first();
        return $bestFighter;
    }
    */
    public function saveFighter($name, $player_id)
    {
        $fightersTable = TableRegistry::get('fighters');
        $surroundingsTable = TableRegistry::get('Surroundings');
        $Surroundings = $surroundingsTable->getSurroundings();
        $fighter = $fightersTable->newEntity();
        
        //Réinitialiser le combattant
        $fighter->name = $name;
        $fighter->player_id = $player_id;
        $fighter->skill_sight = 2;
        $fighter->skill_strength = 1;
        $fighter->skill_health = 5;
        $fighter->current_health = $fighter['skill_health'];
        $fighter->xp = 0;
        $fighter->level = 1;
        $fighter->next_action_time = Time::now();
        list ($lig, $col) = $this->getMaxSize();
        list ($pos_x, $pos_y, $id_enemy) = $this->getPositionFighters();//$id);
        $end=false;
		$t=0;
        $l=0;
        $x=0;
        $y=0;
        while(!$end)
        {
            $x=rand(0,$col-1);
            $y=rand(0,$lig-1);
            $end=true;
            foreach($pos_y as $py)
            {
                foreach($pos_x as $px)
                { 
                    if($l == $t)
                    {
                        if($x == $px->coordinate_x && $y == $py->coordinate_y)
                        {
                             $end = false;
                        }
                    }
                    $l += 1;
                }
                $l=0;
                $t += 1;
            }
            foreach($Surroundings as $s)
            {
                if($x == $s->coordinate_x && $y == $s->coordinate_y)
                {
                     $end = false;
                }
            }
        }
        $fighter->coordinate_x = $x;
        $fighter->coordinate_y = $y;
        
        return $fightersTable->save($fighter);
    }
    public function editFighter($fighter, $sight, $strength, $health)
    {
        $fightersTable = TableRegistry::get('fighters');
        
        //Ajout de la compétance au combattant
        $fighter['skill_sight'] += $sight;
        $fighter['skill_strength'] += $strength;
        $fighter['skill_health'] += 3*$health;

        //Redonner de la vie si la vie augmente
        if($health == 1)
        {
            $fighter['current_health'] = $fighter['skill_health'];
        }
        
        return $fightersTable->save($fighter);
    }
    public function changePlayerPosition($id, $positionX,$positionY)
	{
		$fightersTable = TableRegistry::get('fighters');
		$fighters = $fightersTable->get($id);
        
		$fighters->coordinate_x = $positionX;
		$fighters->coordinate_y = $positionY;
		$fightersTable->save($fighters);
        
	}
    
    public function addXpWhenDead($coord_x,$coord_y,$id)
    {
        $fighters = TableRegistry::get('fighters');
        $me = $fighters->get($id);
        $enemy = $fighters->whoIsOnThisSquare($coord_x,$coord_y);
        $me->xp += $enemy->level-1;
        $fighters->save($me);
    }
    
    public function processDamage($coord_x,$coord_y,$id)
    {
        $fighters = TableRegistry::get('fighters');
        $me = $fighters->get($id);
        $me->xp += 1;
        $fighters->save($me);
        $enemy = $fighters->whoIsOnThisSquare($coord_x,$coord_y);
        $result = $enemy->current_health;
        $damage =  $me->skill_strength;
        $enemy->current_health = $result - $damage;
        $fighters->save($enemy);
    }
    public function isDead($id)
    {
        $fighters = TableRegistry::get('fighters');
        $me = $fighters->get($id);
        if($me->current_health <=0)
        {
            
            return true;
        }
        return false;
    }
    public function attackSuccess($coord_x,$coord_y,$id)
    {
        $x = rand(1,20);
        $fighters = TableRegistry::get('fighters');
        $me = $fighters->get($id);
        $enemy = $fighters->whoIsOnThisSquare($coord_x,$coord_y);
        if(!$enemy)
        {
            return false;
        }
        $result = $enemy->current_health;
        $damage = (10 +  $enemy->level - $me->level);
        //$enemy->current_health = $result - $damage; //(int)$me->level - (int)$enemy->level;// - (10 + $me->level - $enemy->level);
        //$fighters->save($enemy);
        if($x > $damage)
        {
            return true;
        }
        return false;
        
    }
    public function changeFighterLevel($id)
    {
            $fightersTable = TableRegistry::get('fighters');
            $fighters = $fightersTable->get($id);
            $fighters->level;
            $fighters->level +=1;
            //debug($fighters->level);
            $fightersTable->save($fighters);
    }
    
    public function getFighterById($id)
	{
		$fightersTable = TableRegistry::get('fighters');
		$fighters = $fightersTable->find("all", [
                'order' => ['fighters.id' =>'ASC']
            ])->where(['player_id' => $id]);
		$fighter = $fighters->all();
		$x = $fighter->toArray();
		return $x;
	}
    public function whoIsOnThisSquare($coord_x, $coord_y)
    {
        $fighters = TableRegistry::get('fighters');
        try
		{
			$x = $fighters->find("all")->where(['coordinate_x' => $coord_x, 'coordinate_y' => $coord_y]);
            $list = $x->all()->toArray();
		}
        catch(RecordNotFoundException $e)
		{
			$list = [];
		}
        if(!$list)
        {
            return $list;
        }
        else
        {
            return $list[0];
        }
        
    }
	public function getFighterName($id)
	{
		$fightersTable = TableRegistry::get('fighters');
		$fighter = $fightersTable->get($id);
		return $fighter->name;
	}
    public function getPositionFighters()//$id)
    {
        $fighters = TableRegistry::get('fighters');
        $query_x = $fighters->find("all",[
                'order' => ['fighters.id' =>'ASC']
            ])->select(['coordinate_x']);//->where(['id !=' => $id]);
        $query_y = $fighters->find("all",[
                'order' => ['fighters.id' =>'ASC']
            ])->select(['coordinate_y']);//->where(['id !=' => $id]);
        $query_id = $fighters->find("all",[
                'order' => ['fighters.id' =>'ASC']
            ])->select(['player_id']);//->where(['id !=' => $id]);
        
        $result_y = $query_y->all()->toArray();
        $result_x = $query_x->all()->toArray();
        $result_id = $query_id->all()->toArray();
        return array($result_x,$result_y, $result_id);
    }

    
    public function getMaxSize()
    {
        $lig = 10;
        $col = 15;
        return array($lig,$col);
    }
    public function getMaxTime()
    {
        $timePeriod = 10;
        $maxActionPoint = 3;
        //$now = new Time('0000-00-00 00:00:10');
        return array ($timePeriod,$maxActionPoint);
    }
    public function setActionReferenceDate($id,$date)
    {
        $fightersTable = TableRegistry::get('fighters');
        $fighter = $fightersTable->get($id);
        $fighter->next_action_time = $date;
        return $fightersTable->save($fighter);
    }
}